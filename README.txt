Conversation (conversation) module

SUMMARY

Conversation allows turning node comments into one-on-one conversation, locking
out anyone but the node author and author of the first comment.

CONFIGURATION

Node types to have conversation mode comments can be selected under
Configuration > User interface (hey, I had no better idea where to put it).

For each of selected node types, 'start conversation on <type>' permission
becomes available (here, "starting conversation" refers to posting the first
comment).

There is also a global 'post to any conversation' permission that allows just
that - interfering with other users' conversations and annoying them. Not to be
given to known trolls. :)
