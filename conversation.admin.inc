<?php

/**
 * @file
 * Admin interface for Conversation module.
 */

/**
 * Implements hook_form().
 *
 * Configuration form for Conversation module.
 *
 * @ingroup forms
 */
function conversation_configure() {
  $form = array();
  $form['conversation_node_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Node types to have conversation mode comments'),
    '#options'       => node_type_get_names(),
    '#default_value' => variable_get('conversation_node_types', array('')),
    '#description'   => t('These settings will have no effect for nodes where comments are not open.'),
  );
  return system_settings_form($form);
}
